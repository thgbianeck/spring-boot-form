package br.com.bianeck.springboot.form.app.models.domain;

import br.com.bianeck.springboot.form.app.validation.RegexIdentificator;
import br.com.bianeck.springboot.form.app.validation.Required;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;
// import javax.validation.constraints.Pattern;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    public User(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    //@Pattern(regexp = "[0-9]{2}[.,][\\d]{3}[.,][\\d]{3}[-][A-Z]")
    @RegexIdentificator
    private String identificator;

    //@NotEmpty(message = "O nome não pode estar vazio.")
    private String name;

    //@NotEmpty
    @Required
    private String lastName;

    @NotBlank
    @Size(min = 3, max = 8)
    private String username;

    @NotEmpty
    private String password;

    @NotNull
    @Min(5)
    @Max(5000)
    private Integer account;

    @NotNull
    @Future
    @DateTimeFormat(pattern ="yyyy-MM-dd")
    private Date birthDate;

    @NotEmpty
    @Email(message = "E-mail com formato incorreto.")
    private String email;

}
