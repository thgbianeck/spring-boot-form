package br.com.bianeck.springboot.form.app.validation;


import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RequiredValidator implements ConstraintValidator<Required, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !(value == null || !StringUtils.hasText(value));
    }
}
