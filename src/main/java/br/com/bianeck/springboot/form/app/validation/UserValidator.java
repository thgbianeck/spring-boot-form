package br.com.bianeck.springboot.form.app.validation;

import br.com.bianeck.springboot.form.app.models.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        // User user = (User) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required.user.name");

        /*if(!user.getIdentificator().matches("[0-9]{2}[.,][\\d]{3}[.,][\\d]{3}[-][A-Z]")) {
            errors.rejectValue("identificator", "pattern.user.identificator");
        }*/
    }
}
